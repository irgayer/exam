<?php

namespace Database\Factories;

use App\Models\Activity;
use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Activity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $start = Carbon::now()->subHours(rand(1, 5));/*->subDays(rand(1,10))*/;
        $end = Carbon::now()->addHours(rand(3, 9));
        return [
            'user_id' => User::all()->random()->id,
            'task_id' => Task::all()->random()->id,
            'description' => $this->faker->realText(),
            'comment' => $this->faker->realText(),
            'start' => $start,
            'end' => $end
        ];
    }
}
