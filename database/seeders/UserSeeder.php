<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory(10)->create();
        foreach ($users as $user)
            $user->assignRole('regular');

        $data = [
            [
                'login' => 'irgayer',
                'name' => 'Yerbol',
                'password' => Hash::make('1234')
            ],
            [
                'login' => 'dinara',
                'name' => 'Dinara',
                'password' => Hash::make('1234')
            ]
        ];

        $admin = User::create($data[0]);
        $admin->assignRole('admin');

        $manager = User::create($data[1]);
        $manager->assignRole('manager');
    }
}
