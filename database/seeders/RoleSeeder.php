<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'delete users']);
        Permission::create(['name' => 'edit users']);

        Permission::create(['name' => 'create projects']);
        Permission::create(['name' => 'delete projects']);
        Permission::create(['name' => 'edit projects']);

        Permission::create(['name' => 'create tasks']);
        Permission::create(['name' => 'edit tasks']);
        Permission::create(['name' => 'delete tasks']);

        Permission::create(['name' => 'view admin panel']);
        Permission::create(['name' => 'view manager panel']);

        //Permission::create(['name' => 'see users']);
        //Permission::create(['name' => 'see tasks']);
        Permission::create(['name' => 'view statistics']);
        Permission::create(['name' => 'assign tasks']);

        $regular = Role::create(['name' => 'regular']);
        $manager = Role::create(['name' => 'manager']);
        $admin = Role::create(['name' => 'admin']);


        $admin->givePermissionTo(Permission::all());
        $manager->givePermissionTo('view manager panel')->givePermissionTo('assign tasks')->givePermissionTo('view statistics');
    }
}
