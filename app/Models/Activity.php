<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = ['start', 'end', 'description', 'comment', 'task_id'];

    function user() {
        return $this->belongsTo(User::class);
    }

    function task() {
        return $this->belongsTo(Task::class);
    }
}
