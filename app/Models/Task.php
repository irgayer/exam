<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelStatus\HasStatuses;

class Task extends Model
{
    use HasFactory, HasStatuses;

    protected $fillable = ['title', 'deadline', 'description', 'user_id'];

    function project() {
        return $this->belongsTo(Project::class);
    }

    function user() {
        return $this->belongsTo(User::class);
    }

    function activities() {
        return $this->hasMany(Activity::class);
    }

    function assign($user_id) {
        $user_id == 'null' ? $this->update(['user_id' => null]) : $this->update(['user_id' => $user_id]);
    }

    function isValidStatus($name, $reason = null)
    {
        if ($name != 'pending' && $name != 'done')
            return false;

        return true;
    }

    function totalWorktime() {
        $activities = $this->activities()->get();
        /*$total = new Carbon();

        foreach ($activities as $activity)
        {
            $start = Carbon::createFromTimeString($activity->start);
            $end = Carbon::createFromTimeString($activity->end);

            $total->addHours($start->diffInHours($end));
        }

        return $total;*/

        return $this->sumDates($activities);
    }

    function worktime($date = null) {
        $activities = $this->activities()->whereDate('start', '=', $date ? $date : Carbon::today());

        return $this->sumDates($activities);
    }

    private function sumDates($activities) {
        $total = 0;

        foreach ($activities as $activity)
        {
            $start = Carbon::createFromTimeString($activity->start);
            $end = Carbon::createFromTimeString($activity->end);

            $total += $start->diffInHours($end);
        }

        return $total;
    }
}
