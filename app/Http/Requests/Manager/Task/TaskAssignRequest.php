<?php

namespace App\Http\Requests\Manager\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskAssignRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['required'],
            'status' => ['required']
        ];
    }

    function getUserId()
    {
        if ($this->has('user_id') && $this->user_id != 'null')
            return $this->user_id;

        return null;
    }
}
