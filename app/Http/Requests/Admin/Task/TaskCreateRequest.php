<?php

namespace App\Http\Requests\Admin\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'project_id' => ['required'],
            'deadline' => ['required']
        ];
    }

    function getProjectId()
    {
        if ($this->has('project_id'))
            return $this->project_id;

        return null;
    }
}
