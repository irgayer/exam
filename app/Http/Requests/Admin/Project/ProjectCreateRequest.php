<?php

namespace App\Http\Requests\Admin\Project;

use Illuminate\Foundation\Http\FormRequest;

class ProjectCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'manager_id' => ['required']
        ];
    }

    function getManagerId()
    {
        if ($this->has('manager_id'))
            return $this->manager_id;

        return null;
    }
}
