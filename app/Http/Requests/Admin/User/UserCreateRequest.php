<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'login' => ['required'],
            'password' => ['required'],
            'role_id' => ['required']
        ];
    }

    function getRoleId() {
        if ($this->has('role_id'))
            return $this->role_id;

        return null;
    }
}
