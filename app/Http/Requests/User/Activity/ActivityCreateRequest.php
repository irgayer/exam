<?php

namespace App\Http\Requests\User\Activity;

use Illuminate\Foundation\Http\FormRequest;

class ActivityCreateRequest extends FormRequest
{

    public function rules()
    {
        return [
            'description' => ['required'],
            'start' => ['required'],
            'end' => ['required'],
            'task_id' => ['required']
        ];
    }
}
