<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    function index() {
        //dd();
        $tasks = Task::currentStatus('pending');
        $tasks = $tasks->where('user_id', '=', Auth::id())->get();
        return view('user.index', ['tasks' => $tasks]);
    }
}
