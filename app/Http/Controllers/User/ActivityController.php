<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Activity\ActivityCreateRequest;
use App\Models\Activity;
use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    function create($id) {
        return view('user.activity.create', ['task_id' => $id]);
    }

    function store(ActivityCreateRequest $request) {
        //TODO: привести в человеческий вид
        //dd($request->all());
        $data = $request->validated();
        //$data['user_id'] = Auth::id();
        $data['start'] = Carbon::today()->setTimeFromTimeString($data['start']);
        $data['end'] = Carbon::today()->setTimeFromTimeString($data['end']);
        //dd($data);
        $user = Auth::user();
        $user->activities()->create($data);
        //Activity::create($data);

        return redirect()->route('tasks.index');
    }

    function index($date = null) {
        $user = Auth::user();
        //$tasks = $user->tasks()->with('activities')->whereDate('start', '=', Carbon::today())->get();
        $tasks = $user->tasks()->get();

        return view('user.activity.index', ['tasks' => $tasks, 'users' => User::role('regular')->get(), 'user' => $user]);
    }

    function byUser(Request $request) {
        //dd($request->all());
        $user = User::find($request->user_id);
        //dd($user);
        if (!$user)
            return redirect()->route('activities.index');

        $tasks = $user->tasks()->get();
        return view('user.activity.index', ['tasks' => $tasks, 'users' => User::role('regular')->get(), 'user' => $user]);
    }
}
