<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manager\Task\TaskAssignRequest;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Spatie\ModelStatus\Exceptions\InvalidStatus;

class TaskController extends Controller
{
    function index()
    {
        $user = Auth::user();

        //TODO: переделать говно
        $projects = Project::where('user_id', '=', $user->id)->get();
        $tasks = new Collection();
        foreach ($projects as $project)
            $tasks = $tasks->merge($project->tasks()->with('statuses')->get());

        return view('manager.task.index', ['tasks' => $tasks, 'users' => User::role('regular')->get()]);
    }

    function assign($id, TaskAssignRequest $request)
    {
        //TODO: параша
        $data = $request->validated();
        $task = Task::find($id);
        if (!$task)
            return redirect()->route('manager.task.index');

        try {
            $task->setStatus($data['status']);
        } catch (InvalidStatus $exception) {
            dd($exception);
            return  redirect()->route('manager.tasks.index');
        }
        $task->assign($data['user_id']);

        //dd($request->all());

        return redirect()->route('manager.tasks.index');
    }
}
