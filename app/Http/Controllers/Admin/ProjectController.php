<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Project\ProjectCreateRequest;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    function index() {
        //dd(Project::all());
        return view('admin.project.index', ['projects' => Project::all()]);
    }

    function create() {
        return view('admin.project.create', ['managers' => User::role('manager')->get()]);
    }

    function store(ProjectCreateRequest $request)
    {
        $data = $request->validated();
        $user = User::find($request->getManagerId());
        $user->projects()->create($data);

        return redirect()->route('admin.projects.index');
    }
}
