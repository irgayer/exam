<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Task\TaskCreateRequest;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    function index() {
        //dd(Task::all());
        return view('admin.task.index', ['tasks' => Task::all()]);
    }

    function create() {
        return view('admin.task.create', ['projects' => Project::all()]);
    }

    function store(TaskCreateRequest $request) {

        $data = $request->validated();
        $data['project_id'] = $request->getProjectId();

        $project = Project::find($data['project_id']);
        $project->tasks()->create($data);

        //dd($data);
        //Task::create($data);


        return redirect()->route('admin.tasks.index');
    }
}
