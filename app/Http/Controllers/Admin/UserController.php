<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\UserCreateRequest;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    function index() {
        //dd(User::with('roles')->get());
        return view('admin.user.index', ['users' => User::with('roles')->get()]);
    }

    function store(UserCreateRequest $request)
    {
        $data = $request->validated();
        $user = User::create($data);
        $user->assignRole(Role::findById($request->getRoleId()));

        return redirect()->route('admin.users.index');
    }

    function create() {
        return view('admin.user.create', ['roles' => Role::all()]);
    }
}
