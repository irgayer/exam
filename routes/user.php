<?php

use App\Http\Controllers\User\ActivityController;
use App\Http\Controllers\User\TaskController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect()->route('tasks.index');
})->name('home');

Route::group([
    'prefix' => 'tasks',
    'as' => 'tasks.'
], function() {
    Route::get('/', [TaskController::class, 'index'])->name('index');

    Route::group([

    ], function () {
        Route::get('/{id}/activity/create', [ActivityController::class, 'create'])->name('activity.create');
        Route::post('/{id}/activity/create', [ActivityController::class, 'store'])->name('activity.store');
    });
});

Route::group([
    'prefix' => 'activities',
    'as' => 'activities.'
], function() {
    Route::get('/', [ActivityController::class, 'index'])->name('index');
    Route::post('/user', [ActivityController::class, 'byUser'])->name('user');
});


