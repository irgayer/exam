<?php /** @noinspection PhpUndefinedClassInspection */

use App\Http\Controllers\Manager\ManagerController;
use App\Http\Controllers\Manager\TaskController;
use App\Http\Controllers\Manager\UserController;
use Illuminate\Support\Facades\Route;


Route::get('/', [ManagerController::class, 'index'])->name('home');

Route::group([
    'prefix' => 'tasks',
    'as' => 'tasks.'
], function () {
    Route::get('/', [TaskController::class, 'index'])->name('index');
    Route::post('/{id}/assign', [TaskController::class, 'assign'])->name('assign');
});

Route::group([
    'prefix' => 'users',
    'as' => 'users.'
], function() {
    Route::get('/', [UserController::class, 'index'])->name('index');
});

/*Route::get('/manager/tasks', function () {
    return view('manager.task.index', ['tasks' => Task::all(), 'users' => User::all()]);
})->name('manager.tasks');

Route::get('/manager/users', function () {
    return view('manager.user.index', ['users' => User::all()]);
})->name('manager.users');

Route::post('/manager/tasks/{id}/appoint', function ($id) {

})->name('manager.tasks.appoint');

Route::get('/manager', function () {
    return view('manager.index');
})->name('manager.home');*/
