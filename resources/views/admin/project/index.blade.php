@extends('layouts.app')

@section('content')

    <table class="table">
        <tfoot>
        <tr>
            <td>
                <a class="btn btn-primary" href="{{ route('admin.projects.create') }}">Добавить</a>
            </td>
        </tr>
        </tfoot>

        <tr>
            <th scope="col">ID</th>
            <th scope="col">Название</th>
            <th scope="col">Менеджер</th>
        </tr>
        @foreach($projects as $project)
            <tr>
                <th scope="row">{{ $project->id }}</th>
                <td>{{ $project->title }}</td>
                <td>{{ $project->manager->name }}</td>
            </tr>
        @endforeach
    </table>

@endsection
