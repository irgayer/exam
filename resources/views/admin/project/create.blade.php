@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('admin.projects.store') }}">
        @csrf
        <div class="form-group">
            <label for="title">Название</label>
            <input class="form-control"
                   type="text"
                   name="title"
                   id="title">
        </div>

        <div class="form-group">
        <label for="manager">Менеджер</label>
        <select class="form-control" name="manager_id" id="manager">
            @foreach($managers as $manager)
                <option value="{{ $manager->id }}">{{ $manager->name }}</option>
            @endforeach
        </select>
        </div>
        <input class="btn btn-primary" type="submit" value="Готово">
    </form>
@endsection
