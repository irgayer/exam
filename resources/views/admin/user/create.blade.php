@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('admin.users.store') }}">
        @csrf

        <div class="form-group">
        <label for="name">Имя</label>
        <input class="form-control" type="text"
               id="name"
               name="name">
        </div>

        <div class="form-group">
        <label for="login">Логин</label>
        <input class="form-control" type="text"
               id="login"
               name="login">
        </div>
            <div class="form-group">
        <label for="password">Пароль</label>
        <input class="form-control" type="password"
               id="password"
               name="password">
            </div>
        <div class="form-group">
        <label for="role">Роль</label>
        <select class="form-control" name="role_id" id="role">
            @foreach($roles as $role)
            <option value="{{ $role->id }}">{{ $role->name }}</option>
            @endforeach
        </select>
        </div>

        <input class="btn btn-primary" type="submit" value="Готово">
    </form>
@endsection
