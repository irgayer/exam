@extends('layouts.app')

@section('content')
    <table class="table">
        <tfoot>
        <tr>
            <td>
                <a class="btn btn-primary" href="{{ route('admin.users.create') }}">Добавить</a>
            </td>
        </tr>
        </tfoot>

        <tr>
            <th scope="col">ID</th>
            <th scope="col">ФИО</th>
            <th scope="col">Логин</th>
            <th scope="col">Роль</th>
            {{--<th scope="col">Пароль</th>--}}
        </tr>

        @foreach($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->login }}</td>
                <td>@foreach($user->roles as $role) {{ $role->name }}  @endforeach </td>
                {{--<td>
                    <details>{{ Hash::$user->password }}</details>
                </td>--}}
            </tr>
        @endforeach
    </table>
@endsection
