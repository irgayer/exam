@extends('layouts.app')

@section('content')
    <a class="btn btn-primary" href="{{ route('admin.users.index') }}">Пользователи</a>
    <a class="btn btn-primary" href="{{ route('admin.projects.index') }}">Проекты</a>
    <a class="btn btn-primary" href="{{ route('admin.tasks.index') }}">Задачи</a>
@endsection
