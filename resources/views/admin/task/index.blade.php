@extends('layouts.app')

@section('content')

    <table class="table">
        <thead>
        <tr>
            <td>
                <a class="btn btn-primary" href="{{ route('admin.tasks.create') }}">Добавить</a>
            </td>
        </tr>
        </thead>

        <tr>
            <th scope="col">ID</th>
            <th scope="col">Проект</th>
            <th scope="col">Задача</th>
            <th scope="col">Дедлайн</th>
            <th scope="col">Исполнитель</th>
        </tr>
        @foreach($tasks as $task)
            <tr>
                <th scope="row">{{ $task->id }}</th>
                <td>{{ $task->project->title }}</td>
                <td>{{ $task->title }}</td>
                <td>{{ $task->deadline }}</td>
                <td>{{ $task->user ? $task->user->name : "N/A" }}</td>
            </tr>
        @endforeach
    </table>
@endsection
