@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('admin.tasks.store') }}">
        @csrf
        <div class="form-group">
        <label for="title">Название</label>
        <input class="form-control" type="text"
               name="title"
               id="title">
        </div>

        <div class="form-group">
        <label for="deadline">Дедлайн</label>
        <input class="form-control" type="date"
               name="deadline"
               id="deadline">
        </div>

        <div class="form-group">
        <label for="project">Проект</label>
        <select class="form-control" name="project_id" id="project">
            @foreach($projects as $project)
            <option value="{{ $project->id }}">{{ $project->title }}</option>
            @endforeach
        </select>
        </div>

        <input class="btn btn-primary" type="submit" value="Готово">
    </form>
@endsection
