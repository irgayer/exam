@extends('layouts.app')

@section('content')
    <table class="table">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ФИО</th>
            {{--<th scope="col">Пароль</th>--}}
        </tr>

        @foreach($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name }}</td>
                {{--<td>
                    <details>{{ Hash::$user->password }}</details>
                </td>--}}
            </tr>
        @endforeach
    </table>
@endsection
