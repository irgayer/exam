@extends('layouts.app')

@section('content')
    {{--Проекты
    <ul>
    @foreach($projects as $project)
        <li>{{ $project->name }}</li>
    @endforeach
    </ul>--}}
    <a class="btn btn-primary" href="{{ route('manager.tasks.index') }}">Задачи</a>
    <a class="btn btn-primary" href="{{ route('manager.users.index') }}">Пользователи</a>
@endsection
