@extends('layouts.app')

@section('content')
    <style>
        DIV.table
        {
            display:table;
        }
        FORM.tr, DIV.tr
        {
            display:table-row;
        }
        SPAN.td
        {
            display:table-cell;
        }
        SPAN.th
        {
            display: table-header-group;
        }
    </style>

    <div class="table">
        <div class="tr">
            <span class="td">Проект</span>
            <span class="td">Задача</span>
            <span class="td">Дедлайн</span>
            <span class="td">Исполнитель</span>
            <span class="td">Статус</span>
        </div>
        @foreach($tasks as $task)
        <form class="tr" method="post" action="{{ route('manager.tasks.assign', ['id' => $task->id]) }}">
            @csrf
            <input type="hidden" name="id" value="{{ $task->id }}">
            <span class="td">{{ $task->project->title }}</span>
            <span class="td">{{ $task->title }}</span>
            <span class="td">{{ $task->deadline }}</span>
            <span class="td">
                <select name="user_id">
                    <option value="null">N/A</option>
                    @foreach($users as $user)
                        <option value={{ $user->id }}
                        @if ($user->id == $task->user_id)
                        selected="selected"
                        @endif>{{ $user->name }}</option>
                    @endforeach
                </select>
            </span>
            <span class="td">
                <select name="status">
                    <option value="pending" @if($task->status == 'pending') selected @endif>В работе</option>
                    <option value="done" @if($task->status == 'done') selected @endif>Сделано</option>
                </select>
            </span>
            <input type="submit" class="btn btn-primary" value="Готово">
            {{--<span class="td"><input type="text"/></span>--}}
        </form>
        @endforeach

    </div>
    {{--<table>
        <tr>
            <th>Проект</th>
            <th>Задача</th>
            <th>Дедлайн</th>
            <th>Исполнитель</th>
        </tr>
        @foreach($tasks as $task)
            <tr>
                <td>{{ $task->project->title }}</td>
                <td>{{ $task->title }}</td>
                <td>{{ $task->deadline }}</td>
                <td>{{ $task->user ? $task->user->name : "N/A" }}</td>
            </tr>
        @endforeach
    </table>--}}
@endsection
