@extends('layouts.app')

@section('content')
    <table class="table">
        <tr>
            <th scope="col">Проект</th>
            <th scope="col">Задача</th>
            <th scope="col">Дедлайн</th>
        </tr>
        @foreach($tasks as $task)
            <tr>
                <td>{{ $task->project->title }}</td>
                <td><a href="{{ route('tasks.activity.create', ['id' => $task->id]) }}">{{ $task->title }}</a></td>
                <td>{{ $task->deadline }}</td>
            </tr>
        @endforeach
    </table>
@endsection
