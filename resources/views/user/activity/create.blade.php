@extends('layouts.app')

@section('content')
    <form method="post" action="{{ route('tasks.activity.store', $task_id) }}">
        @csrf
        <input type="hidden" name="task_id" value="{{ $task_id }}">

        <div class="form-group">
        <label for="description">Описание работы</label>
        <textarea class="form-control" name="description" id="description"></textarea>
        </div>

        <div class="form-group">
        <label for="start">С</label>
        <input class="form-control" type="time" name="start" id="start">
        </div>

        <div class="form-group">
        <label for="end">До</label>
        <input class="form-control" type="time" name="end" id="end">
        </div>

        <div class="form-group">
        <label for="comment">Комментарий</label>
        <textarea class="form-control" name="comment" id="comment"></textarea>
        </div>

        <input class="btn btn-primary" type="submit" value="Готово">
        <a class="btn btn-danger" href="{{ route('tasks.index') }}">Отмена</a>
    </form>
@endsection
