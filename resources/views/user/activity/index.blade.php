@extends('layouts.app')

@section('content')
    <h1>{{ $user->name }}</h1>
    <form method="get" action="{{ route('activities.index') }}">
        <input type="date" name="date" id="date" value="{{ date('Y-m-d') }}">
        <input class="btn btn-primary" type="submit" value="Показать">
    </form>
    <form method="post" action="{{ route('activities.user') }}">
        @csrf
        <select name="user_id" id="user_id">
            <option value="{{ Auth::id() }}" selected>Я</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach
        </select>
        <input class="btn btn-primary" type="submit" value="Показать">
    </form>
    </thead>
    <table class="table">
        <tr>
            <th scope="col">Проект</th>
            <th scope="col">Задача</th>
            <th scope="col">Дедлайн</th>
            {{--<th scope="col">Время работы сегодня</th>--}}
            <th scope="col">Общее время работы</th>
            <th scope="col">Статус задачи</th>
        </tr>
        @foreach($tasks as $task)
            <tr>
                <td>{{ $task->project->title }}</td>
                <td>{{ $task->title }}</td>
                <td>{{ $task->deadline }}</td>
                {{--<td>{{ $task->worktime() }}</td>--}}
                <td>{{ $task->totalWorktime() }}</td>
                <td>{{ $task->status }}</td>
            </tr>
        @endforeach

    </table>
@endsection
